INC = include
LIB_NAME = CSP
LIB = lib$(LIB_NAME).so
LIB_SRC_FOLDER = lib_src
LIB_OBJ_FOLDER = lib_obj
LIB_SRC = $(wildcard $(LIB_SRC_FOLDER)/*.cpp)
LIB_OBJ = $(LIB_SRC:$(LIB_SRC_FOLDER)/%.cpp=$(LIB_OBJ_FOLDER)/%.o)
ifeq ($(TYPE), debug)
	CFLAGS = -std=c++17 -g -O0 -I $(INC) -pthread
else
	CFLAGS = -std=c++17 -s -O2 -I $(INC) -pthread
endif

all : mkdirs so

buildlib : $(LIB_OBJ)

so : buildlib
	c++ -shared -fPIC -o $(LIB) $(LIB_OBJ)

$(LIB_SRC_FOLDER)/%.cpp : $(INC)/%.hpp

$(LIB_OBJ_FOLDER)/%.o : $(LIB_SRC_FOLDER)/%.cpp
	c++ $(CFLAGS) -fPIC -c $< -o $@

mkdirs :
	mkdir -p $(LIB_OBJ_FOLDER)

clean:
	rm $(LIB_OBJ) $(LIB)

install :
	mkdir -p /usr/include/$(LIB_NAME)
	cp $(INC)/* /usr/include/$(LIB_NAME)
	cp lib$(LIB_NAME).so /usr/lib/


.PHONY: clean mkdirs