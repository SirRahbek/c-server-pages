#include "HTTPRequest.hpp"


HTTPRequest::HTTPRequest () {
	return;
}


HTTPRequest::HTTPRequest (const string& req) {
	if (req == "")
		return;
	//Start by cleaning up after windows.
	string request = regex_replace(req, regex("\r\n"), "\n");

	auto head_body = split(request, "\n\n");
	auto data = split(head_body[0]/*head*/, "\n");
	//Message body comes after first empty newline.
	body = (head_body.size() > 1) ? head_body[1] : "";

	//First process first line, containing type, uri and protocol.
	vector<string> t = split(data[0], " ");

	type     = (t.size()>0) ? t[0] : "";
	uri      = (t.size()>1) ? t[1] : "";
	protocol = (t.size()>2) ? t[2] : "";
	page     = uri;

	//See if the header is illegal before continuing.
	if (SharedData::requestTypes.count(req) == 0)
		return;

	//Erase first line, and process rest as  'TYPE: DATA'
	data.erase(data.begin());
	for (string field : data) {
		if (field == "") break;

		int stop = field.find_first_of(':');
		if (stop == string::npos || stop > field.length())
			emplace(field, "");
		else
			emplace(field.substr(0,stop), field.substr(stop+2));
	}
	return;
}


//I assume it's a file request if the last entry (/../last), contains a dot '.'
bool HTTPRequest::isFileRequest () {
	for (auto it=uri.rbegin() ; it!=uri.rend() ; ++it) {
		if (*it == '.')
			return true;
		if (*it == '/')
			return false;
	}
	return false;
}


HTTPResponse HTTPRequest::getFile () {
	HTTPResponse response;

	string filename = SharedData::getRootDir() + uri.substr(1);
	fstream file (filename);

	if (file.is_open()) {
		response.status = HTTPResponse::OK;
		response.page = fileToString(&file);

		//Return the correct MIME type.
		string suffix = getSuffix(uri);
		if (response.mimeTypes.count(suffix) == 1)
			response.content_type = response.mimeTypes[suffix];
		else
			response.content_type = "text/html";
	}
	else {
		response.status = HTTPResponse::NOT_FOUND;
	}

	return response;
}

