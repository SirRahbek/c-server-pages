#include "Pager.hpp"

//==========================================================================
//===============================| Page |===================================
//==========================================================================



HTTPResponse Page::optionsCallback (Session* s, HTTPRequest* r) { return HTTPResponse(); }
HTTPResponse Page::getCallback (Session* s, HTTPRequest* r)     { return HTTPResponse(); }
HTTPResponse Page::headCallback (Session* s, HTTPRequest* r)    { return HTTPResponse(); }
HTTPResponse Page::postCallback (Session* s, HTTPRequest* r)    { return HTTPResponse(); }
HTTPResponse Page::putCallback (Session* s, HTTPRequest* r)     { return HTTPResponse(); }
HTTPResponse Page::deleteCallback (Session* s, HTTPRequest* r)  { return HTTPResponse(); }
HTTPResponse Page::traceCallback (Session* s, HTTPRequest* r)   { return HTTPResponse(); }
HTTPResponse Page::connectCallback (Session* s, HTTPRequest* r) { return HTTPResponse(); }

HTTPResponse Page::errorCallback (Session* s, HTTPRequest* r, ErrorMsg* e) { 
	return HTTPResponse();
}


Page::Page () {
	return;
}
 
Page::Page (const Page& other) {
	return;
}


HTTPResponse Page::handleRequest (Session* s, HTTPRequest* r) {
	if(strcmp(r->type.c_str(),"OPTIONS") == 0) return optionsCallback(s,r);
	if(strcmp(r->type.c_str(),"GET") == 0)     return getCallback(s,r);
	if(strcmp(r->type.c_str(),"HEAD") == 0)    return headCallback(s,r);
	if(strcmp(r->type.c_str(),"POST") == 0)    return postCallback(s,r);
	if(strcmp(r->type.c_str(),"PUT") == 0)     return putCallback(s,r);
	if(strcmp(r->type.c_str(),"DELETE") == 0)  return deleteCallback(s,r);
	if(strcmp(r->type.c_str(),"TRACE") == 0)   return traceCallback(s,r);
	if(strcmp(r->type.c_str(),"CONNECT") == 0) return connectCallback(s,r);
	
	return errorCallback(s,r, new ErrorMsg(ErrorMsg::INTERNAL_ERROR, "Unknown request type")); 
}




//==========================================================================
//==============================| Pager |===================================
//==========================================================================

Pager::Pager () : Page() {
	return;
}

Pager::Pager (const Pager& other) : Page(other) {
	pages = other.pages;
	return;
}


bool Pager::add (string name, Page* page) {
	if (pages.count(name) > 0)
		return false;
	pages[name] = page;
	return true;
}


HTTPResponse Pager::handleRequest (Session* s, HTTPRequest* r) {
	try {
		if (r->page=="" || r->page=="/") {
			return Page::handleRequest(s,r);
		}
		//NOTE: this might be optional for the server. Move switch to options?
		if (r->isFileRequest()) {
			printf("File request\n");
			return r->getFile();
		}

		//Get the page name if any exists.
		string page = getPrefix(r->page.substr(1),'/');
		
		if ( (page.length() > 0) && (pages.count(page) > 0) ) {
			printf("page %s in %s\n", page.c_str(), r->page.c_str());
			r->page.erase(0, page.length()+1); //Chomp the page we try to access before forwarding.
			return pages[page]->handleRequest(s,r);
		}
		else {
			printf("page %s not found\n", page.c_str());
			return errorCallback(s, r, new ErrorMsg(ErrorMsg::NOT_FOUND, "Page not found"));
		}
	}
	catch (const exception& e) {
		return errorCallback(s, r, new ErrorMsg(ErrorMsg::INTERNAL_ERROR, "Internal server error"));
	}
	
}