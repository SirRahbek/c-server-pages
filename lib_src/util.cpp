#include "util.hpp"


vector<string> split (const string& str, const string& delimiter) {
	vector<string> result;
	string temp;

	for (int n=0 ; n<str.size() ; ++n) {
		if (str[n] == delimiter[0]) {
			for (int m=0 ; str[n+m] == delimiter[m] ; ++m) {
				if (m == delimiter.size()-1) {
					result.push_back(temp);
					temp.clear();
					break;
				}
			}
		}
		else
			temp += str[n];
	}

	if (temp != "")
		result.push_back(temp);

	return result;
}


string getSuffix (string s, char delim) {
	string output;
	for (auto it=s.rbegin() ; it!=s.rend() ; ++it) {
		if (*it == delim)
			break;
		output += *it;
	}
	reverse(output.begin(),output.end());
	return output;
}


string getPrefix (string s, char delim) {
	string output;
	for (char c : s) {
		if (c == delim)
			break;
		output += c;
	}
	return output;
}



string fileToString (string file) {
	fstream _file(file);
	string output = (_file.is_open()) ? fileToString( &_file ) : "";
	_file.close();
	return output;
}


string fileToString (fstream* file) {
	string output;

	file->seekg(0, std::ios::end);   
	output.reserve(file->tellg());
	file->seekg(0, std::ios::beg);

	output.assign(
		std::istreambuf_iterator<char>(*file),
		std::istreambuf_iterator<char>()
	);

	return output;
}