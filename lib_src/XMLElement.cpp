#include "XMLElement.hpp"


//==========================================================================
//============================| XMLElement |================================
//==========================================================================
string XMLElement::tabSymbol = "  ";

XMLElement::XMLElement () {
	return;
}

XMLElement::~XMLElement () {
	return;
}

XMLElement::XMLElement (const XMLElement& other) {
	return; 
}

XMLElement* XMLElement::copy () const {
	return new XMLElement(*this);
}


void XMLElement::clear () {
	return;
}

string XMLElement::toString (int level) {
	return "";
}

string XMLElement::TAB (int level) {
	string output = "";
	for (int i=0 ; i<level ; ++i) output += tabSymbol;
	return output;
}




//==========================================================================
//=============================| XMLNode |==================================
//==========================================================================
XMLNode::XMLNode () {
	return;
}

XMLNode::XMLNode (const XMLNode& other) : XMLElement(other),
	name(other.name), attributes(other.attributes), content(other.content)
{
	for (int i : other.ownedContent) {
		content[i] = other.content[i]->copy();
		ownedContent.push_back(i);
	}
	return;
}

/*XMLNode::XMLNode (XMLNode&& other) :
	content(other.content), ownedContent(other.ownedContent), 
	attributes(other.attributes), name(other.name)
{
	other.ownedContent.clear();
	return;
}*/

XMLNode::XMLNode (string name) : name(name) {
	return;
}

XMLNode::XMLNode (string name, Attributes attributes) : name(name), attributes(attributes) {
	return;
}

XMLNode::~XMLNode () {
	for (int i: ownedContent) {
		delete(content[i]);
		content[i] = NULL;
	}
	return;
}

XMLElement* XMLNode::copy () const {
	return (XMLElement*) new XMLNode(*this);
}

void XMLNode::clear () {
	for (int i : ownedContent)
		delete(content[i]);
	ownedContent.clear();
	content.clear();
}


bool XMLNode::addAttribute (string attr, string value) {
	bool wasThere = (attributes.count(attr)==0);
	attributes.at(attr) = value;
	return wasThere;
}

XMLNode XMLNode::add (const XMLElement& element) {
	XMLElement* l = element.copy();
	ownedContent.push_back(content.size());
	content.push_back(l);
	return *this;
}


XMLNode XMLNode::add (XMLElement* node) {
	content.push_back(node);
	return *this;
}

XMLNode XMLNode::add (string leaf) {
	XMLLeaf* l = new XMLLeaf(leaf);
	ownedContent.push_back(content.size());
	content.push_back(l);
	return *this;
}

XMLList XMLNode::forAll (string name) {
	XMLList list;
	regex reg(name);
	for (XMLElement* e : content) {
		if (instanceof<XMLNode>(e)) {
			XMLNode* node = (XMLNode*)e;
			if (regex_match(node->name,reg))
				list.addToList(node);
		}
	}
	return list;
}


string XMLNode::toString (int level) {
	string output="";

	output += TAB(level) + "<"+name+placeAttributes()+">\n";
	for (XMLElement* e : content)
		output += e->toString(level+1);
	output += TAB(level) + "</"+name+">\n";

	return output;
}


string XMLNode::placeAttributes () {
	string output = "";

	for (auto a : attributes) {
		if (a.second=="")
			output += " "+a.first;
		else
			output += " "+a.first+"=\""+a.second+"\"";
	}

	return output;
}


XMLNode& XMLNode::operator= (XMLNode& other) {
	if (this != &other) {
		name         = other.name;
		attributes   = other.attributes;
		content      = other.content;
		ownedContent = other.ownedContent;
	}
	return *this;
}


XMLList XMLNode::operator[] (const string name) {
	return forAll(name);
}

XMLNode XMLNode::operator() (const XMLElement& e) {
	return add(e);
}

XMLNode XMLNode::operator() (XMLElement* e) {
	return add(e);
}

XMLNode XMLNode::operator() (string l) {
	return add(l);
}



//==========================================================================
//=============================| XMLLeaf |==================================
//==========================================================================
XMLLeaf::XMLLeaf (const XMLLeaf& other) : XMLElement(other) {
	content = other.content;
	return;
}

XMLLeaf::XMLLeaf (string content) {
	this->content = content;
	return;
}

XMLLeaf::~XMLLeaf () {
	return;
}

XMLElement* XMLLeaf::copy () const {
	return (XMLElement*) new XMLLeaf(content);
}

void XMLLeaf::clear () {
	content.clear();
}

string XMLLeaf::toString (int level) {
	return TAB(level) + content + "\n";
}




//==========================================================================
//=============================| XMLList |==================================
//==========================================================================

XMLList::XMLList () : XMLNode("__LIST__") {
	return;
}

XMLList::XMLList (const XMLList& other) : XMLNode(other) {
	content = other.content;
	return;
}

XMLElement* XMLList::copy () const {
	return new XMLList(*this);
}


XMLList XMLList::addToList (XMLElement* e) {
	if (instanceof<XMLNode>(e)) {
		XMLNode* node = (XMLNode*) e;
		add(node);
	}	
	return *this;
}


XMLList XMLList::add (const XMLLeaf& node) {
	for (auto e : content) {
		if (instanceof<XMLNode>(e)) {
			XMLNode* n = (XMLNode*) e; 
			n->add( XMLLeaf(node) );
		}
	}
	return *this;
}


XMLList XMLList::add (const XMLNode& node) {
	for (auto e : content) {
		if (instanceof<XMLNode>(e)) {
			XMLNode* n = (XMLNode*) e; 
			n->add( XMLNode(node) );
		}
	}
	return *this;
}


XMLList XMLList::add (XMLElement* node) {
	for (auto e : content) {
		if (instanceof<XMLNode>(e)) {
			XMLNode* n = (XMLNode*) e; 
			n->add( node );
		}
	}
	return *this;
}


XMLList XMLList::add (string leaf) {
	for (auto e : content) {
		if (instanceof<XMLNode>(e)) {
			XMLNode* n = (XMLNode*) e; 
			n->add( XMLLeaf(leaf) );
		}
	}
	return *this;
}


XMLList XMLList::forAll (string name) {
	XMLList list;
	regex reg(name);
	for (auto e : content) {
		if (!instanceof<XMLNode>(e))
			continue;
		XMLNode* node_e = (XMLNode*) e;
		for (auto f : node_e->content) {
			if (!instanceof<XMLNode>(f))
				continue;
			XMLNode* node_f = (XMLNode*)f;
			if (regex_match(node_f->name,reg))
				list.addToList(node_f);
		}
	}
	return list;
}


vector<XMLElement*>::iterator XMLList::begin () {
	return content.begin();
}

vector<XMLElement*>::iterator XMLList::end () {
	return content.end();
}

int XMLList::size() {
	return content.size();
}


string XMLList::toString (int level) {
	string output = "";
	for (auto e : content) {
		output += e->toString(level);
	}
	return output;
}


/*XMLList& XMLList::operator= (XMLList other) {
	if (this != &other) {
		name       = other.name;
		attributes = other.attributes;
		content    = other.content;
	}
	return *this;
}*/


XMLList XMLList::operator[] (const string name) {
	return forAll(name);
}

XMLList XMLList::operator() (const XMLNode& e) {
	return add(e);
}

XMLList XMLList::operator() (const XMLLeaf& e) {
	return add(e);
}

XMLList XMLList::operator() (XMLElement* e) {
	return add(e);
}

XMLList XMLList::operator() (string l) {
	return add(l);
}