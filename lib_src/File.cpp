#include "File.hpp"

File::File (const File& f) {
	this->name = f.name;
	this->path = f.path;
	return;
}

File::File (const string& name, const string& path) {
	this->name = name;
	this->path = path;

	if (this->path.back() != '/')
		this->path += "/";
	return;
}

std::string File::nameOnly (char delim) {
	return name.substr(0, name.find_last_of(delim));
}


std::string File::read () {
	std::ifstream in(path+name);
	std::string content (
		(std::istreambuf_iterator<char>(in)), 
		std::istreambuf_iterator<char>()
	);
	return content;
}

void File::write (const std::string& str) {
	std::ofstream out;
	out.open (path+name);
	out << str;
	out.close();
	return;
}



Directory::Directory (const string& _path) : File(_path, _path) {
	DIR* dir = opendir(path.c_str());

	for (dirent* entry=readdir(dir) ; entry ; entry=readdir(dir)) {
		if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, ".."))
			files.push_back( File(entry->d_name, path) );
	}

	closedir(dir);
	return;
}


vector<File>::iterator Directory::begin () {
	return files.begin();
}


vector<File>::iterator Directory::end () {
	return files.end();
}