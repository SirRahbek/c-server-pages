#include "HTMLTools.hpp"


namespace HTMLTools
{

	//===========================================================================
	// HTMLNode
	//===========================================================================
	HTMLNode::HTMLNode (const XMLNode& other) : XMLNode(other) 
	{}

	HTMLNode::HTMLNode (const HTMLNode& other) : XMLNode(other), 
		id(other.id), classes(other.classes)
	{}

	HTMLNode::HTMLNode (string name) : XMLNode(name) {
		return;
	}

	HTMLNode::HTMLNode (string name, Attributes attributes) : XMLNode(name,attributes) {
		return;
	};

	XMLElement* HTMLNode::copy () const {
		return new HTMLNode(*this);
	}

	HTMLNode HTMLNode::setId (string id) {
		this->id = id;
		return *this;
	}

	HTMLNode HTMLNode::addClass (string c) {
		classes.insert(c);
		return *this;
	}

	string HTMLNode::toString (int level) {
		string classString = "";
		for (string c : classes)
			classString += c + " ";

		if (classString.length() > 0)
			attributes["class"] = classString;
		if (id.length() > 0)
			attributes["id"]    = id;
		
		return XMLNode::toString(level);
	}



	//===========================================================================
	// HTML
	//===========================================================================
	HTML::HTML () : HTMLNode("html"), head("head"), body("body") {
		add(&head);
		add(&body);
		return;
	}

	HTML::HTML (const HTML& other) : HTMLNode(other), head(other.head), body(other.body) {
		add(&head);
		add(&body);
		return;
	}

	XMLElement* HTML::copy () const {
		return new HTML(*this);
	}



	//===========================================================================
	// Div
	//===========================================================================
	Div::Div () : HTMLNode("div") { 
		return;
	}

	Div::Div (const Div& other): HTMLNode(other) 
	{}

	Div::Div (string id) : HTMLNode("div") { 
		this->id = id;
		return;
	}

	Div::Div (string id, Attributes attributes) : HTMLNode("div") {
		this->attributes = attributes;
		this->id = id;
		return;
	}

	XMLElement* Div::copy () const {
		return new Div(*this);
	}



	//===========================================================================
	// P
	//===========================================================================
	P::P () : HTMLNode("div") 
	{}

	P::P (const P& other) : HTMLNode("p") 
	{}

	XMLElement* P::copy () const {
		return new P(*this);
	}




	//===========================================================================
	// List
	//===========================================================================
	List::List () : HTMLNode("ul") { 
		return; 
	};

	List::List (string id) :HTMLNode("ul") {
		setId(id);
		return;
	}

	XMLElement* List::copy () const {
		return new List(*this);
	}

	string List::toString (int level) {
		string classString = "";
		for (string c : classes)
			classString += c + " ";

		if (classString.length() > 0)
			attributes["class"] = classString;
		if (id.length() > 0)
			attributes["id"]    = id;
		
		string output="";

		output += TAB(level) + "<"+name+placeAttributes()+">\n";
		for (XMLElement* e : content)
			output += "<li>" + e->toString(level+1) + "</li>";
		output += TAB(level) + "</"+name+">\n";

		return output;
	}

} //End of namespace HTMLTools