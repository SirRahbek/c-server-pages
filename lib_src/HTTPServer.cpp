#include "HTTPServer.hpp"


HTTPServer::HTTPServer() : ServerSocket() , logfileName("server.log") {
	printf("Setup socket %i as CSP page\n", thisSocket);

	FILE* logfile = fopen(logfileName.c_str(), "ab+");
		auto t = time(0);
		tm* tm = localtime(&t);
		fprintf(logfile, "<Server started %d/%d/%d>\n", 
			tm->tm_year+1900,
			tm->tm_mon + 1,
			tm->tm_mday
		);
	fclose(logfile);

	return;
}

int HTTPServer::start () {
	int socketStart = ServerSocket::start(SharedData::port);
	if (socketStart != 0)
		return socketStart;

	signal(SIGINT,  &HTTPServer::stop_callback);
	signal(SIGABRT, &HTTPServer::stop_callback);

	running = true;
	while (running) {
		usleep(1000); //Don't hog all the cpu.
		string result;
		PSocket* client = poll();

		if (client == NULL)
			continue;

		if (client->revents & POLLIN) {
			thread* thread = NULL;
			thread = new std::thread( &HTTPServer::handleRequest, this, client );
			thread->join();
			delete thread; //This is safe for some reason...
			//handleRequest(client);
			client->revents -= POLLIN;
		}
		if (client->revents & POLLOUT) {
			//NOTHING
			client->revents -= POLLOUT;
		}
	}
	printf("Closing CSP\n");
	return 0;
}


int HTTPServer::handleRequest (PSocket* client) {
	clock_t begin = clock();

	HTTPResponse response;
	client->events ^= POLLIN; //Disable reading on this socket.

	char* buffer;
	client->pollData((void**)&buffer, 10000);
	HTTPRequest request(buffer);

	if (request.isFileRequest()) {
		response = request.getFile();
	}
	else {
		Session* session = new Session();
		response = (request.isFileRequest())
			? Page::handleRequest(session, &request)
			: Pager::handleRequest(session, &request);
		delete session;
	}
	
	string result = response.buildResponse();

	//WARNING: this will only work when server is in threaded mode.
	while (!(client->revents & POLLOUT)) 
		{/*Wait for POLLOUT to allow writing data*/}
	
	client->sendData(result.c_str(), result.length()+1);
	
	free(buffer);
	client->events ^= POLLIN; //reenable reading from the socket.
	
	FILE* logfile = fopen(logfileName.c_str(), "ab+");
	fprintf(logfile, "%02d  [%s]  %-8s %-25s %-10s %.2fms\n", 
		client->thisSocket, client->getIP().c_str(),
		request.type.c_str(), request.uri.c_str(), request.protocol.c_str(),
		double(clock() - begin) / (CLOCKS_PER_SEC/1000)
	);
	fclose(logfile);
	return 0;
}


bool HTTPServer::running = false;

void HTTPServer::stop_callback (int garbage) {
	printf("Server interrupted: %d\n", garbage);
	HTTPServer::running = false;
	return;
}