#include <HTTPSharedData.hpp>



int    SharedData::port      = 80;
string SharedData::serverDir = "srv"; //These folder are used as:
string SharedData::CSPDir    = "CSP"; //   /serverDir/CSPDir/
bool   SharedData::systemWideServer = false;

set<string> SharedData::requestTypes = {
	"OPTIONS", "GET",
	"HEAD"   , "POST",
	"PUT"    , "DELETE",
	"TRACE"  , "CONNECT"
};


string SharedData::getRootDir () {
	if (systemWideServer)
		return "/" + serverDir + "/" + CSPDir + "/";
	else
		return "";
}