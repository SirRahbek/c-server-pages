#include "Socket.h"



//=============================================================================
//=================================| Socket |==================================
//=============================================================================
Socket::Socket () {
	thisSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (thisSocket == -1)
		printf("Socket cound not be opened :(\n");
	if (thisSocket != 0) 
		printf("Creating socket %i\n", thisSocket);
	return;
}

Socket::Socket (int sock) {
	thisSocket = sock;
	return;
}

void Socket::connectToServer (int port, char* addressString) {
	sockaddr_in info;

	memset(&info, '0', sizeof(info));
	info.sin_family = AF_INET;
	info.sin_port = htons(port);

	inet_pton(AF_INET, addressString, &info.sin_addr);

	connect(thisSocket, (sockaddr*)&info, sizeof(info));
	return;
}

int Socket::pollData (void** data, int chunksize) {
	errno = 0;
	int size; 
	*data = (char*) malloc(chunksize);
	size = recv(thisSocket, *data, chunksize, 0);
	
	int err = errno;
	if (size == -1)
		printf("recv errno: %d %s\n", err, strerror(err));
	return size;
}


int Socket::sendData (const void* data, int length, bool sendAll) {
	errno = 0;
	int sent=0;

	if (sendAll) {
		for (int total=0 ; total<length ; total+=sent) {
			sent = send(thisSocket, (const void*)(((long)data)+total), length-total, 0);
			if (sent==-1) break;
		}
		sent = (sent==-1) ? sent : length;
	}
	else {
		sent = send(thisSocket, data, length, 0);
	}

	int err = errno;
	if (sent == -1)
		printf("send errno: %d %s\n", err, strerror(err));

	return sent;
}


int Socket::close () {
	//printf("Closing socket %i\n", thisSocket);
	return ::close(thisSocket);
}

string Socket::getIP () {
	sockaddr_in  address;
	socklen_t    address_length;
	char*        clientip = new char[20];

	if ( getpeername(thisSocket,(sockaddr*)&address, &address_length) )
		return "ERROR " + errno;
	
	strcpy(clientip, inet_ntoa(address.sin_addr));

	string str(clientip);
	delete clientip;
	return str;
}




//=============================================================================
//=============================| Polling Socket |==============================
//=============================================================================
PSocket::PSocket () : Socket() {
	return;
}

PSocket::PSocket  (int s, short events, short revents) : Socket(s) {
	this->events  = events;
	this->revents = revents;
	return;
}

PSocket::PSocket (const PSocket& other) : Socket(other.thisSocket) {
	events  = other.events;
	revents = other.revents;
	return;
}


//This must be called after reading or sending to a psocket.
void PSocket::doneReading () {
	revents = 0;
	return;
}


//=============================================================================
//==============================| ServerSocket |===============================
//=============================================================================
ServerSocket::ServerSocket () : PSocket() {
	printf("Socket %i is server\n", thisSocket);
	events  = POLLIN;
	revents = 0;

	//Make the application able to quick restart with same socket.
	int optional = 1;
	setsockopt(thisSocket, SOL_SOCKET, SO_REUSEADDR, &optional, sizeof optional);
	return;
}

ServerSocket::~ServerSocket () {
	for (PSocket& s : clients)
		s.close();
	return;
}

int ServerSocket::start (int port) {
	sockaddr_in	info;
	memset(&info, 0, sizeof(info));

	info.sin_family			= AF_INET;
	info.sin_addr.s_addr		= htonl(INADDR_ANY);
	info.sin_port				= htons(port);

	if (fcntl(thisSocket, F_SETFL, O_NONBLOCK|fcntl(thisSocket,F_GETFL,0)) < 0)
		return -1;
	if (bind(thisSocket, (sockaddr*) &info, sizeof(info)) < 0)
		return -2;
	if (listen(thisSocket, 10) < 0)
		return -3;

	clients.push_back( PSocket(thisSocket, events, revents) );
	return 0;
}

PSocket* ServerSocket::poll (int timeout) {
	int changes = ::poll((pollfd*)clients.data(), clients.size(), timeout);

	if (changes < 0) {
		printf("Error in poll: %d\n", errno);
		return NULL;
	}
	if (changes == 0)
		return NULL;

	for (int i=0 ; i<clients.size() ; i++) {
		PSocket* c = &clients[i];
		if (c->revents == 0) {
			continue;
		}
		else if (c->revents & POLLIN) {
			if (c->thisSocket == thisSocket) {
				int newClient = accept(thisSocket, NULL, NULL);
				if (newClient>0) {
					clients.push_back( PSocket(newClient, POLLIN | POLLOUT) );
					return &clients.back();
				}
				else return c;
			}
			else {
				int datacount;
				ioctl(c->thisSocket, FIONREAD, &datacount);
				if (datacount == 0) {
					c->close();
					clients.erase(clients.begin()+i);
					--i; //Adjust for newly closed client.
					return NULL;
				}
				else
					return c;
			}
		}
		else if (c->revents & POLLPRI) {
			printf("Priority unband data on socket %d\n", c->thisSocket);
		}
		else if (c->revents & POLLOUT) {
			/* Do nothing. This property is checked elsewhere */
		}
		else if (c->revents & POLLERR) {
			printf("Socket event error: %d", errno);
		}
		else if (c->revents & POLLHUP) {
			return NULL;
		}
		else if (c->revents & POLLNVAL) {
			printf("Unvalid request from socket: %d\n", c->thisSocket);
		}
		else {
			printf("Unhandleable revent type: %d\n", c->revents);
			c->close();
			clients.erase(clients.begin()+i);
			--i;
		}
	}
	return NULL;
}
