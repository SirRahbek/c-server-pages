#include "HTTPResponse.hpp"


map<string,string> HTTPResponse::mimeTypes = {
	//Text files
	{"css" , "text/css"},
	{"c"   , "text/plain"},
	{"cpp" , "text/plain"},
	{"c++" , "text/plain"},
	{"htm" , "text/html"},
	{"html", "text/html"},
	{"txt" , "text/plain"},
	{"text", "text/plain"},
	{"xml" , "text/xml"},
	{"js"  , "text/javascript"},
	//Images
	{"jpg" , "image/jpeg"},
	{"jpeg", "image/jpeg"},
	{"png" , "image/png"},
	{"bmp" , "image/bmp"},
	//Sound
	{"mp3" , "audio/mpeg3"},
	{"wav" , "audio/wav"},
	//Video
	{"flv" , "video/flv"},
	{"mp4" , "video/mp4"},
	{"mov" , "video/quicktime"},
	{"avi" , "video/x-msvideo"},
	//Compressed stuff
	{"tar" , "application/x-tar"},
	{"zip" , "application/zip"}
};



string HTTPResponse::serverName = "Serverus Snape 0.01a";

HTTPResponse::HTTPResponse () {
	page            = "";
	status          = STATUS::OK;
	content_type    = "text/html";
	connection_type = "closed";
	return;
}


HTTPResponse::HTTPResponse (const HTTPResponse& other) {
	page            = other.page;
	status          = other.status;
	content_type    = other.content_type;
	connection_type = other.connection_type;
	return;
}


string HTTPResponse::buildResponse () {
	return 
		"HTTP/1.1 "         + to_string(status) + "\n"
		"Server: "          + serverName + "\n" +
		"Content-Length: "  + to_string(page.length()) + "\n" +
		"Content-Type: "    + content_type + "\n" +
		"Connection: "      + connection_type + "\n\n" +
		page;
}