#pragma once

#include "Socket.h"
#include "Session.hpp"
#include "HTTPRequest.hpp"
#include "HTTPResponse.hpp"
#include "HTTPSharedData.hpp"
#include "ErrorMsg.hpp"
#include "Pager.hpp"
#include <signal.h>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <thread>
#include <map>
#include <string>

using namespace std;

class HTTPServer : public ServerSocket, public Pager {
	static bool running;
	map<int, Session*> sessions; //map a session to each IP.
	string logfileName;

public:
	HTTPServer ();

	int start ();

protected:
	int handleRequest (PSocket* client);

	static void stop_callback (int);
};