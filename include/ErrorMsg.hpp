#pragma once
#include <exception>
#include <string>

using namespace std;


class ErrorMsg : public exception {
public:
	enum {
		NOT_FOUND = 404,
		INTERNAL_ERROR = 500
	};

	int    code;
	string message;

	ErrorMsg (int code);
	ErrorMsg (int code, string message);

	string what ();
};
