#pragma once

#include <string>
#include <set>
#include "XMLElement.hpp"

using namespace std;

namespace HTMLTools {
	
	class HTMLNode : public XMLNode {
	protected:
		string      id;
		set<string> classes;
	public:
		HTMLNode (const XMLNode& other);
		HTMLNode (const HTMLNode& other);
		HTMLNode (string name);
		HTMLNode (string name, Attributes attributes);

		virtual XMLElement* copy () const;

		HTMLNode setId    (string id);
		HTMLNode addClass (string c);

		virtual string toString (int level=0);
	};


	class HTML : public HTMLNode {
	public:
		HTMLNode head, body;

		HTML ();
		HTML (const HTML& other);

		virtual XMLElement* copy () const;
	};


	class Div : public HTMLNode {
	public:
		Div ();
		Div (const Div& other);
		Div (string id);
		Div (string id, Attributes attributes);

		virtual XMLElement* copy () const;
	};


	class P : public HTMLNode {
	public:
		P ();
		P (const P& other);

		virtual XMLElement* copy () const;
	};


	class List : public HTMLNode {
	public:
		List ();
		List (string id);
		virtual string toString (int level=0);

		virtual XMLElement* copy () const;
	};


	inline HTMLNode CSS (string file) {
		return HTMLNode("link", {{"rel","stylesheet"},{"type","text/css"},{"href",file}});
	};

	inline HTMLNode Script (string file) {
		return HTMLNode("script", {{"src",file}});
	};

	inline HTMLNode Image (string file) {
		return HTMLNode("img", {{"src", file}});
	}

	inline HTMLNode Link (string href) {
		return HTMLNode("a", {{"href", href}})(href);
	}

	inline HTMLNode Link (string href, const XMLNode& content) {
		return HTMLNode("a", {{"href", href}})(content);
	}
}