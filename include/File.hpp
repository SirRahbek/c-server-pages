#pragma once
#include <dirent.h>
#include <string.h>
#include <string>
#include <vector>
#include <fstream>


using namespace std;

class File {
	public:
		string name;
		string path;

		File (const File& f);
		File (const string& name, const string& path="./");

		std::string nameOnly (char delim='.');

		virtual string read  ();
		virtual void   write (const std::string& str);
};


class Directory : public File {
		vector<File> files;
	public:
		Directory (const string& path);

		vector<File>::iterator begin ();
		vector<File>::iterator end ();
};