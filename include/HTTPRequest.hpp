#pragma once

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <streambuf>
#include <algorithm>

#include "util.hpp"
#include "HTTPResponse.hpp"
#include "HTTPSharedData.hpp"
#include "util.hpp"

using namespace std;


class HTTPRequest : public map<string,string> {
public:
	string type;
	string uri;
	string page; //some suffix of the uri, used with paging.
	string protocol;
	string body;

	HTTPRequest ();
	HTTPRequest (const string& request);

	bool isFileRequest ();
	HTTPResponse getFile ();
};