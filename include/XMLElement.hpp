#pragma once

#include <string>
#include <vector>
#include <map>
#include <regex>
#include <initializer_list>
#include "util.hpp"

using namespace std;

class XMLElement;
class XMLNode;
class XMLLeaf;
class XMLList;

using NodeContent   = vector<XMLElement*>; 
using Attributes    = map<string,string>;


//==========================================================================
//============================| XMLElement |================================
//==========================================================================
class XMLElement {
protected:
	static string tabSymbol;

	string TAB (int level);

public:
	XMLElement ();
	XMLElement (const XMLElement& other);

	virtual ~XMLElement ();
	virtual XMLElement* copy () const;
	virtual void clear ();
	virtual string toString (int level=0); 
};



//==========================================================================
//=============================| XMLNode |==================================
//==========================================================================
class XMLNode : public XMLElement {
protected:
	string         name;
	Attributes     attributes;

	string placeAttributes ();

public:
	NodeContent content;
	vector<int> ownedContent; //denotes which indexes in content that we own.

	XMLNode ();
	XMLNode (const XMLNode& other);
	//XMLNode (XMLNode&& other);
	XMLNode (string name);
	XMLNode (string name, Attributes attributes);
	virtual ~XMLNode ();

	virtual XMLElement* copy () const;

	virtual void clear ();

	bool addAttribute(string attr, string value);

	XMLNode add (const XMLElement& element);
	XMLNode add (XMLElement* element);
	XMLNode add (string leaf);
	virtual XMLList forAll (string name);

	virtual string toString (int level=0);

	XMLNode& operator= (XMLNode& other);

	virtual XMLList operator[] (const string name);	//Has same behaviour as forAll(name)
	XMLNode operator() (const XMLElement& e);			//Equal to add(...)
	XMLNode operator() (XMLElement* e);
	XMLNode operator() (string l);

	friend XMLList;
};



//==========================================================================
//=============================| XMLLeaf |==================================
//==========================================================================
class XMLLeaf : public XMLElement {
	string content;

public:
	XMLLeaf (const XMLLeaf& other);
	XMLLeaf (string content);
	virtual ~XMLLeaf ();

	virtual XMLElement* copy () const;
	virtual void clear ();
	virtual string toString (int level=0);
};



//==========================================================================
//=============================| XMLList |==================================
//==========================================================================
class XMLList : public XMLNode {

public:
	XMLList ();
	XMLList (const XMLList& other);

	virtual XMLElement* copy () const;

	XMLList add (const XMLLeaf& node);
	XMLList add (const XMLNode& node);
	XMLList add (XMLElement* node);
	XMLList add (string leaf);

	XMLList forAll (string name);

	vector<XMLElement*>::iterator begin ();
	vector<XMLElement*>::iterator end ();
	int size();

	virtual string toString (int level=0);

	XMLList& operator= (XMLList other);

	XMLList operator[] (const string name);	//Has same behaviour as forAll(name)
	XMLList operator() (const XMLNode& e);		//Equal to add(...)
	XMLList operator() (const XMLLeaf& e);		// ...
	XMLList operator() (XMLElement* e);
	XMLList operator() (string l);


protected:
	virtual XMLList addToList (XMLElement* e);

	friend XMLNode;
};