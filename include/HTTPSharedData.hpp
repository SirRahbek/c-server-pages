#pragma once
#include <string>
#include <set>
#include "HTTPResponse.hpp"
#include "HTTPRequest.hpp"

using namespace std;

class SharedData {
public:
	static int    port;
	static string serverDir;
	static string CSPDir;
	static bool   systemWideServer;

	static set<string> requestTypes;

	static string getRootDir ();
};