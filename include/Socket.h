#pragma once

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/poll.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <functional>
#include <vector>
#include <string>
#include <utility>

using namespace std;

//=============================================================================
//=================================| Socket |==================================
//=============================================================================
class Socket 
{
public:
	int       thisSocket;

	Socket  ();
	Socket  (int);

	void	 connectToServer (int port, char* addressString);
	int	 pollData        (void** buffer, int chunksize=1024);
	int	 sendData        (const void* data, int length, bool sendAll=true);
	int    close           ();
	string getIP           ();
};


//=============================================================================
//=============================| Polling Socket |==============================
//=============================================================================
class PSocket : public Socket 
{
public:
	//Alongside the socket fd, this makes a pollfd strucure, now as a class :3
	short events;
	short revents;

	PSocket  ();
	PSocket  (const PSocket& other);
	PSocket  (int s, short events=0, short revents=0);

	void doneReading ();
};



//=============================================================================
//==============================| ServerSocket |===============================
//=============================================================================
class ServerSocket : public PSocket
{
public:
	vector<PSocket> clients;
	vector<pair<PSocket,string>> returns;

	ServerSocket ();
	~ServerSocket ();
	int start (int port);

	PSocket* poll (int timeout=0);
};
