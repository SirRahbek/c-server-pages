#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <streambuf>
#include <algorithm>
#include <regex>
#include <typeinfo>


using namespace std;



template<typename Base, typename T>
inline bool instanceof(const T *ptr) {
    return dynamic_cast<const Base*>(ptr) != nullptr;
}

template<typename T, typename K>
inline bool isType(const K &k) {
    return typeid(T).hash_code() == typeid(k).hash_code();
}

vector<string> split (const string& str, const string& delimiters);

string getPrefix (string s, char delim='.');
string getSuffix (string s, char delim='.');

string fileToString (string file);
string fileToString (fstream* file);
