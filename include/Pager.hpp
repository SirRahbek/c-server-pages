#pragma once

#include <set>
#include <functional>
#include <exception>
#include "HTTPRequest.hpp"
#include "HTTPResponse.hpp"
#include "ErrorMsg.hpp"
#include "Session.hpp"

using namespace std;
using HTTPCallback = function<HTTPResponse(Session*, HTTPRequest*)>;

class Page {
public:
	//The set of standard HTTP requests, as defined by W3.
	virtual HTTPResponse optionsCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse getCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse headCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse postCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse putCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse deleteCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse traceCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse connectCallback (Session* s, HTTPRequest* r);
	virtual HTTPResponse errorCallback (Session* s, HTTPRequest* r, ErrorMsg* e);

	Page ();
	Page (const Page& other);

	virtual HTTPResponse handleRequest (Session* s, HTTPRequest* r);
};



class Pager : public Page {
	map<string,Page*> pages;
public:
	Pager ();
	Pager (const Pager& other);

	bool add (string name, Page* page);
	virtual HTTPResponse handleRequest (Session* s, HTTPRequest* r);
};