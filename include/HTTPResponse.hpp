#pragma once

#include <string>
#include <map>

using namespace std;


class HTTPResponse {
public:
	enum STATUS {
		OK           = 200,
		BAD_REQUEST  = 400,
		UNAUTHORIZED = 401,
		FORBIDDEN    = 402,
		NOT_FOUND    = 404,
		SERVER_ERROR = 500
	};

	static string serverName;
	static map<string,string> mimeTypes;

	string page;
	int    status;
	string content_type;
	string connection_type;

	HTTPResponse ();
	HTTPResponse (const HTTPResponse& other);
	string buildResponse ();
};