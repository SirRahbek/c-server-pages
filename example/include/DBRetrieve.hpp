#include <CSP/ServerPages.hpp>
#include <sqlite3.h>


class DBRetrieve : public Page {
	sqlite3* database;
	static string database_name = "database";

public:
	DBRetrieve () {
		sqlite3_open(database_name, &database);
		return;
	}

	~DBRetrieve () {
		sqlite3_close(database);
		return;
	}

	HTTPResponse getCallback (Session* s, HTTPRequest* r) {
		HTTPResponse response;
		char* errormsg = NULL;
		int status = sqlite3_exec(database, "NONE", SQLiteCallback, errormsg);

		response.page = 
			"Query returned:\n" + 
			query_response;

		sqlite3_free(errormsg);
		return response;
	}

	static int SQLiteCallback (void* garbage, int argc, char* argv[], char* column[]) {
		return 0;
	}
};