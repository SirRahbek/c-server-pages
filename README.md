# The C++ Server Pages library #

The C++ Server Pages (CSP) library adds HTTP server functionality to C++,
and facilitates paging much in the same way JSP or ASP would do.
(I've been informed this is called a REST API)

As of now the library is very much only in the alpha state, so do
expect some errors and crashes. 

Also the library was made for - and on - Linux, and has not been
tested anywhere else.


### Using the library ###
I have not written a tutorial on the library, but there is a
small example website under example/


### Building the damned thing ###

The build procedure is your standard make -- make install, which
will yield a libCSP.so in your system you can link to. The headers
will be put under CSP in the include folder.